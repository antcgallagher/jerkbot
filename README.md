# jerkbot

Telegram bot for displaying quotes from jerkcity/bonequest

# Initial Setup

When checking out fresh you will need to run the following scripts to retrieve the .html files from bonequest in order to strip the quotes

`python3 getjerk.py`

`python3 stripquotes.py`

# Running the bot

To run the bot you must have [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) installed. Follow the instructions on the page to install the library. Please note if installing from source you should run `python3 install.py` as the bot does not run on Python 2.

Once installed, you will need to supply a telegram bot api token in a file called `api.token` in the same directory as `jerkbot.py`.

Then simply run `python3 jerkbot.py` to start.

# Running in the background

If you want your bot to run in the background, it is recommended to use the linux command screen.

* Run `screen`
* Then start the bot `python3 jerkbot.py`
* Press CTRL-A+D to detach from the screen session
* Then you may close the shell

To resume later when logged into a shell, run `screen -r` to return to the running session. You can then stop the bot or do whatever else may be needed.

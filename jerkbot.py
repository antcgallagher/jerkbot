import logging
import time
import random
from telegram.ext import Updater, MessageHandler, Filters

COOLDOWN = 21600

# Get API token from api.token
token_filename = 'api.token'
with open(token_filename) as f:
    TOKEN = f.readline()

# Strip off any unnecessary whitespace
TOKEN = TOKEN.strip()

# Get quotes from jerkcity.txt
quote_filename = "jerkcity.txt"
with open(quote_filename) as f:
    quotes = list(f)

# Strip unnecessary padding whitespace
quotes = [x.strip() for x in quotes]

# Initialise timestamps
chat_timestamps = dict()

def cooldown_finished(update):
    chat_id = update.message.chat_id
    current_timestamp = time.time()
    if (chat_id in chat_timestamps):
        timestamp = chat_timestamps[chat_id]
        if (current_timestamp - timestamp < COOLDOWN):
            # Cooldown is still going, return false
            return False
        else:
            # Cooldown is finished, so reset it to current time
            return True
    else:
        # If chat does not have a timestamp, this a new chat, add it to the
        # dictionary with current time as timestamp
        return True
        
def handle_jerk(update, context):
    if (cooldown_finished(update)):
        chat_timestamps[update.message.chat_id] = time.time()
        quote = random.choice(quotes)
        context.bot.send_message(chat_id=update.message.chat_id, text=quote)

def handle_time(update, context):
    chat_id = update.message.chat_id
    if (chat_id in chat_timestamps):
        timestamp = chat_timestamps[chat_id]
        current_timestamp = time.time()
        if (current_timestamp - timestamp < COOLDOWN):
            remaining_cooldown = COOLDOWN - (current_timestamp - timestamp)
            hours = remaining_cooldown / 3600
            remaining_cooldown = remaining_cooldown % 3600
            minutes = remaining_cooldown / 60
            remaining_cooldown = remaining_cooldown % 60
            seconds = remaining_cooldown 

            message = "Cooldown: %d h %d m %d s" % (hours, minutes, seconds)
            context.bot.send_message(chat_id=chat_id, text=message)
        else:
            message = "Ready to !jerk it"
            context.bot.send_message(chat_id=chat_id, text=message)
    else:
        message = "Ready to !jerk it"
        context.bot.send_message(chat_id=chat_id, text=message)

def jerk(update, context):
    message = update.message
    if (message is not None):
        print("Received message %s from %s" % (message.text, message.chat_id))
        if (update.message.text == "!jerk"):
            handle_jerk(update, context)
        elif (update.message.text == "!time"):
            handle_time(update, context)

updater = Updater(token=TOKEN, use_context=True)

dispatcher = updater.dispatcher

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

jerk_handler = MessageHandler(Filters.text, jerk)
dispatcher.add_handler(jerk_handler)

updater.start_polling()

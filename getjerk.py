from urllib import request, error

i = 1

while True:
    url = "http://www.bonequest.com/" + str(i) + ".html"

    print(url)

    try:
        file_name = "jerkcity/" + url.split('/')[-1]
        file_data = request.urlopen(url)
        data_to_write = file_data.read()

        with open(file_name, 'wb') as f:
            f.write(data_to_write)
        
        i+=1
    except error.HTTPError:
        break

import os.path
import re

i = 1

output_file = "jerkcity.txt"
dialog_pattern = "(?<=span class=\"toiletpig\"\>).*(?=\n\<\/span)"
regex = re.compile(dialog_pattern, flags=re.DOTALL)
filter_chars = 'abcdefghijklmnopqrstuvwxyz'

# Clear output file initially
open(output_file, 'w').close()

file_name = "jerkcity/" + str(i) + ".html"

while True:
    file_name = "jerkcity/" + str(i) + ".html"
    
    if not (os.path.isfile(file_name)):
        break 

    print(file_name)

    with open(file_name, 'r') as f:
        raw_text = f.read()

    dialog = regex.search(raw_text)

    # Not all strips have dialog
    if dialog and i != 30:
        dialog = dialog.group(0)

        # Filter dialog
        dialog = re.sub(r'\n:', r'. ', dialog)
        dialog = re.sub(r'[a-z]+:', r'', dialog)
        dialog = re.sub(r'(?<=[A-Z\)])(\n)+', r'. ', dialog)
        dialog = re.sub(r'(\n)+', r' ', dialog)

        ## TODO: DO THIS IN A CLEANER WAY ##
        dialog = re.sub(r'&#39;', r"'", dialog)
        dialog = re.sub(r'&#34;', r"'", dialog)
        dialog = re.sub(r'&amp;', r"&", dialog)

        with open(output_file, 'a') as f:
            f.write(dialog + '\n')

    i+=1
